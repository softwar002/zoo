package zooteam.rulesset

import zooteam.data.animals.AnimalI
import zooteam.data.area.Cage
import zooteam.data.area.Lake
import zooteam.data.area.Zone
import zooteam.data.area.base.BasicArea

class AviaryProvider {

    fun provideAviary(animal: AnimalI): List<BasicArea> {

        val hashSet = ArrayList<BasicArea>()

        when (animal.animalName()) {
            "kitsia" -> {
                val cage = Cage(true)
                cage.x = 2.0
                cage.y = 3.0
                val lake = Lake()
                lake.x = 3.0
                lake.y = 4.0
                val zone = Zone()
                zone.x = 3.0
                zone.y = 4.0
                hashSet.addAll(listOf(cage, lake, zone))
            }
            "monkey" -> {
                val cage = Cage(false)
                cage.x = 1.5
                cage.y = 1.5
                hashSet.addAll(listOf(cage))
            }
        }
        return hashSet
    }

}