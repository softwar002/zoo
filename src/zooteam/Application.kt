package zooteam

import zooteam.test.ZooTest

class Application

fun main() {
    val zooTest = ZooTest()
    zooTest.proceedFullFlow();
}