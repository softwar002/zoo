package zooteam.data.area.base

import zooteam.calculation.base.CalculationI

open class BasicArea: CalculationI<Double> {
    var x: Double? = null
    var y: Double? = null

    override fun getArea(): Double {
        return this.x?.times(this.y ?: 1.0) ?: 0.0
    }

}