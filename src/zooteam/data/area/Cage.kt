package zooteam.data.area

import zooteam.data.area.base.BasicArea

data class Cage(val isTall: Boolean): BasicArea()