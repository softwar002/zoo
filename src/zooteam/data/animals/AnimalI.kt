package zooteam.data.animals

interface AnimalI {
    fun foodType(): String
    fun animalName(): String
}