package zooteam.data.animals.movementStyle

interface RunMovementI: Movement {
    fun run(): String
}