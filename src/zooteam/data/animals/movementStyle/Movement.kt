package zooteam.data.animals.movementStyle

interface Movement {
    fun type(): String
}