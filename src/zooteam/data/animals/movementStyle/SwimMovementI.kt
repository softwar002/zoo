package zooteam.data.animals.movementStyle

interface SwimMovementI: Movement {
    fun swim(): String
}