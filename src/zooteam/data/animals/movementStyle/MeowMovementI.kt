package zooteam.data.animals.movementStyle

interface MeowMovementI : Movement{
    fun meow(): String
}