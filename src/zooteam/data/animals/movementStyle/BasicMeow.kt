package zooteam.data.animals.movementStyle

class BasicMeow: MeowMovementI {
    override fun meow(): String = "I would like to meow, please provide me a Cage"
    override fun type(): String = "meow"
}