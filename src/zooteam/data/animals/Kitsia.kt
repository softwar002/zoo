package zooteam.data.animals

import zooteam.data.animals.movementStyle.MeowMovementI
import zooteam.data.animals.movementStyle.RunMovementI
import zooteam.data.animals.movementStyle.SwimMovementI

class Kitsia : AnimalI{

    override fun foodType() = "apples"
    override fun animalName() = "kitsia"

}