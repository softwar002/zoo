package zooteam.test

import zooteam.data.animals.Kitsia
import zooteam.data.animals.Monkey
import zooteam.data.area.Lake
import zooteam.rulesset.AviaryProvider
import zooteam.service.AviaryCalculator

class ZooTest {

    fun calculateLake(){
        val lake = Lake()
        lake.x = 21.00
        lake.y = 21.00
        val area = lake.getArea()
        println(area)
    }

    fun proceedFullFlow(){
        val aviaryProvider = AviaryProvider()
        val kitsiaAviary = aviaryProvider.provideAviary(Kitsia())
        val monkeyAviary = aviaryProvider.provideAviary(Monkey())
        val aviaryCalculator = AviaryCalculator()
        val kitsiaAviaryArea = aviaryCalculator.calc(kitsiaAviary)
        val monkeyAviaryArea = aviaryCalculator.calc(monkeyAviary)
        println(kitsiaAviaryArea)
        println(monkeyAviaryArea)
    }

}