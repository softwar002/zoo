package zooteam.calculation.base

interface CalculationI<I: Number> {

    fun getArea(): Double

}