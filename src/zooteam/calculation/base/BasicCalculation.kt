package zooteam.calculation.base

abstract class BasicCalculation {

    fun getArea(x: Double, y: Double): Double = x.times(y)
}