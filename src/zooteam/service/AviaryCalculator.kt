package zooteam.service

import zooteam.data.area.base.BasicArea

class AviaryCalculator {

    fun calc(areas: List<BasicArea>): Double {
        return areas.stream()
            .mapToDouble { it.getArea() }
            .sum()
    }

}